#!/usr/bin/env python
from __future__ import division
from scipy import sparse
from scipy.sparse import csr_matrix, hstack

from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from xgboost import XGBClassifier
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
from const import N_CLASS, SEED
from kaggler.data_io import load_data, save_data


def generate_feature(train_file_all, test_file_all, train_m3_feature_file,
                     test_m3_feature_file, train_feature_file, test_feature_file):
    """Using m3 features + https://www.kaggle.com/alexxanderlarko/bosch-production-line-performance/road-2-0-4-featureset/code """
    logging.info('loading all train set')

    X, y_trn = load_data(train_m3_feature_file)
    f_magic = pd.read_csv('data/Farons_train_features.csv.gz')
    X_trn = hstack((X[:, 1:], f_magic.iloc[:, 1:].values), format='csr')

    # load test data
    X, y_tst = load_data(test_m3_feature_file)
    f_magic = pd.read_csv('data/Farons_test_features.csv.gz')
    X_tst = hstack((X[:, 1:], f_magic.iloc[:, 1:].values), format='csr')

    logging.debug('all features: train: {}, test: {}'.format(X_trn.shape, X_tst.shape))

    logging.info('saving as HDF5 format')
    save_data(X_trn, y_trn, train_feature_file)
    save_data(X_tst, y_tst, test_feature_file)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-all', required=True, dest='train_file_all')
    parser.add_argument('--test-file-all', required=True, dest='test_file_all')
    parser.add_argument('--train-m3-feature-file', required=True, dest='train_m3_feature_file')
    parser.add_argument('--test-m3-feature-file', required=True, dest='test_m3_feature_file')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_all,
                     args.test_file_all,
                     args.train_m3_feature_file,
                     args.test_m3_feature_file,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

