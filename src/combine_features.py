#!/usr/bin/env python
from __future__ import division
from scipy import sparse
import argparse
import logging
import numpy as np
import os
import time


from kaggler.data_io import load_data, save_data


def generate_feature(feature_file1, feature_file2, feature_file):

    logging.info('loading base features')
    X1, y = load_data(feature_file1)
    X2, _ = load_data(feature_file2)
    logging.info('shape of features: {}, {}'.format(X1.shape, X2.shape))

    logging.info('saving combined features')
    if sparse.issparse(X1) or sparse.issparse(X2):
        save_data(sparse.hstack((X1, X2), format='csr'), y, feature_file)
    else:
        save_data(np.hstack((X1, X2)), y, feature_file)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--feature-file1', required=True, dest='feature_file1')
    parser.add_argument('--feature-file2', required=True, dest='feature_file2')
    parser.add_argument('--feature-file', required=True, dest='feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.feature_file1, args.feature_file2, args.feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

