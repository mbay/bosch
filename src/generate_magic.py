#!/usr/bin/env python
from __future__ import division
from scipy import sparse
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from xgboost import XGBClassifier
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
from const import N_CLASS, SEED
from scipy.sparse import csr_matrix, hstack


from kaggler.data_io import load_data, save_data


def f(x):
    t = x.split('_')
    return '{}_{}'.format(t[0], t[1])


def generate_feature(train_file_all,
                     test_file_all,
                     train_feature_file,
                     test_feature_file):
    """Generate original magic features from script https://www.kaggle.com/mmueller/bosch-production-line-performance/road-2-0-4/code """

    logging.info('loading all train set')
    ID_COLUMN = 'Id'
    TARGET_COLUMN = 'Response'
    X_trn, y_trn = load_data(train_file_all)
    train = pd.DataFrame(columns=['Id', 'Response'], index=range(0, X_trn.shape[0]))
    train.Id = X_trn[:, 0].todense()
    train.Response = y_trn

    X_tst, y_tst = load_data(test_file_all)
    test = pd.DataFrame(columns=['Id'], index=range(0, X_tst.shape[0]))
    test.Id = X_tst[:, 0].todense()

    train["StartTime"] = -1
    test["StartTime"] = -1

    colnames = pd.read_csv('data/colnames.txt', header=None, squeeze=True)
    dt_inds = range(np.where(colnames == 'L0_S0_D1')[0][0], np.where(colnames == 'L3_S51_D4255')[0][0])

    # get the start times
    X_trn_date = X_trn[:, dt_inds].todense()
    X_trn_date[X_trn_date == 0] = 1e16
    train["StartTime"] = np.min(X_trn_date, axis=1)

    X_tst_date = X_tst[:, dt_inds].todense()
    X_tst_date[X_tst_date == 0] = 1e16
    test['StartTime'] = np.min(X_tst_date, axis=1)

    # Calculate magic features
    ntrain = train.shape[0]
    train_test = pd.concat((train, test)).reset_index(drop=True).reset_index(drop=False)

    train_test['magic1'] = train_test[ID_COLUMN].diff().fillna(9999999).astype(int)
    train_test['magic2'] = train_test[ID_COLUMN].iloc[::-1].diff().fillna(9999999).astype(int)

    train_test = train_test.sort_values(by=['StartTime', 'Id'], ascending=True)

    train_test['magic3'] = train_test[ID_COLUMN].diff().fillna(9999999).astype(int)
    train_test['magic4'] = train_test[ID_COLUMN].iloc[::-1].diff().fillna(9999999).astype(int)

    train_test = train_test.sort_values(by=['index']).drop(['index'], axis=1)
    train = train_test.iloc[:ntrain, :]
    test = train_test.iloc[ntrain:, :]

    feat_names = ['magic1', 'magic2', 'magic3', 'magic4']

    logging.debug('all features: train: {}, test: {}'.format(X_trn.shape, X_tst.shape))

    logging.info('saving as HDF5 format')
    save_data(csr_matrix(train[feat_names].values), y_trn, train_feature_file)
    save_data(csr_matrix(test[feat_names].values), y_tst, test_feature_file)
    logging.info('saved.')


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-all', required=True, dest='train_file_all')
    parser.add_argument('--test-file-all', required=True, dest='test_file_all')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_all,
                     args.test_file_all,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

