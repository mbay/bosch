#!/usr/bin/env python
from __future__ import division
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
from scipy.sparse import csr_matrix

from kaggler.data_io import load_data, save_data

def _get_time_range(row, time_index):
    row_dt = np.squeeze(np.asarray(row[0,time_index].todense()))
    row_dt_valid = row_dt[row_dt > 0]

    if len(row_dt_valid) > 0:
        dt_s = np.min(row_dt_valid)
        dt_e = np.max(row_dt_valid)
        return dt_s, dt_e, dt_e - dt_s
    else:
        return -1,-1,-1 

def _generate_feature(raw_file, feature_file, colnames, index_date_features, index_date_lines):
    logging.info('loading raw data')
    X, y = load_data(raw_file)

    # skip ID
    X = X[:, 1:]
    
    dt_range = np.empty(shape=(X.shape[0], 3))
    dt_line_range = np.empty(shape=(X.shape[0], 4*3))

    for idx in range(0,X.shape[0]):
        row = X[idx, :]
        dt_range[idx, 0], dt_range[idx, 1], dt_range[idx, 2] = _get_time_range(row, index_date_features)
        for i in range(4):
            dt_line_range[idx, i*3+0], dt_line_range[idx, i*3+1], dt_line_range[idx, i*3+2] = _get_time_range(row, index_date_lines[i])

    logging.info('saving features')
    X = csr_matrix(np.hstack((dt_range, dt_line_range)))
    logging.debug('dim(X): {}'.format(X.shape))

    save_data(X, y, feature_file)


def generate_feature(train_file_all, test_file_all, train_feature_file, test_feature_file):
    logging.info('loading column names')
    colnames = pd.read_csv('data/colnames.txt', header=None, squeeze=True)
    colnames = colnames[1:]
    colnames.index = range(0, len(colnames))

    index_date_features = colnames.index[colnames.apply(lambda x: True if 'D' in x else False)].values

    index_date_lines = []
    for line_number in xrange(4):
        index_date_lines.append( colnames.index[colnames.apply(lambda x: True if 'D' in x and 'L{0}'.format(line_number) in x else False)].values)

    logging.info('generating features for training data')
    _generate_feature(train_file_all, train_feature_file, colnames, index_date_features, index_date_lines)

    logging.info('generating features for test data')
    _generate_feature(test_file_all, test_feature_file, colnames, index_date_features, index_date_lines)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-all', required=True, dest='train_file_all')
    parser.add_argument('--test-file-all', required=True, dest='test_file_all')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_all,
                     args.test_file_all,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

