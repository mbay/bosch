#!/usr/bin/env python
from __future__ import division
from scipy import sparse
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from xgboost import XGBClassifier
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
from const import N_CLASS, SEED
from scipy.sparse import csr_matrix, hstack


from kaggler.data_io import load_data, save_data


def f(x):
    t = x.split('_')
    return '{}_{}'.format(t[0], t[1])


def generate_data(file_name, colnames):
    X, y = load_data(file_name)
    X = X[:, 1:]  # do not include Id

    dt_inds = range(927, 1088)
    dt_colnames = colnames[dt_inds]

    row_ls = pd.Series(index=np.unique(colnames.apply(f)))
    all_ls = np.empty(shape=(X.shape[0], len(row_ls)))
    dt_range = np.empty(shape=(X.shape[0], 3))

    for idx in range(0,X.shape[0]):
        row = X[idx, :]
        nz = list(row.nonzero()[1])
        row_ls = pd.Series(index=np.unique(colnames.apply(f)))
        nz_ls = colnames[nz].apply(f).unique()
        row_ls[nz_ls] = 1
        all_ls[idx, :] = row_ls.values

        row_dt = np.squeeze(np.asarray(row[0,dt_inds].todense()))

        if len(row_dt[row_dt > 0]) > 0:
            dt_range[idx, 0] = np.min(row_dt[row_dt > 0])
            dt_range[idx, 1] = np.max(row_dt[row_dt > 0])
            dt_range[idx, 2] = dt_range[idx, 1] - dt_range[idx, 0]
        else:
            dt_range[idx, 0] = 0
            dt_range[idx, 1] = 0
            dt_range[idx, 2] = 0
 
    X_sps = hstack((X, np.nan_to_num(all_ls), np.nan_to_num(dt_range)), format='csr')
    return X_sps, y


def generate_feature(train_file_all,
                     test_file_all,
                     train_feature_file,
                     test_feature_file,
                     magic_train_feature_file,
                     magic_test_feature_file):
    """Generate features from script https://www.kaggle.com/joconnor/bosch-production-line-performance/python-xgboost-starter-0-209-public-mcc """
    logging.info('loading all train set')


    colnames = pd.read_csv('data/colnames.txt', header=None, squeeze=True)
    colnames = colnames[1:]
    colnames.index = range(0, len(colnames))

    dt_inds = range(928, 1089)
    dt_colnames = colnames[dt_inds]
    dt_colnames.index = range(0, len(dt_colnames))
    lines_dt = [[0, 37], [37, 123], [123, 127], [127, 162]]


    X_trn, y_trn = generate_data(train_file_all, colnames)
    f_magic_trn = pd.read_csv(magic_train_feature_file)
    X_trn = hstack((X_trn[:, 1:], f_magic_trn.iloc[:, 1:].values), format='csr')


    # load test data
    X_tst, y_tst = generate_data(test_file_all, colnames)
    f_magic_tst = pd.read_csv(magic_test_feature_file)
    X_tst = hstack((X_tst[:, 1:], f_magic_tst.iloc[:, 1:].values), format='csr')

    logging.debug('all features: train: {}, test: {}'.format(X_trn.shape, X_tst.shape))

    logging.info('saving as HDF5 format')
    save_data(X_trn, y_trn, train_feature_file)
    save_data(X_tst, y_tst, test_feature_file)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-all', required=True, dest='train_file_all')
    parser.add_argument('--test-file-all', required=True, dest='test_file_all')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')
    parser.add_argument('--magic-train-feature-file', required=True, dest='magic_train_feature_file')
    parser.add_argument('--magic-test-feature-file', required=True, dest='magic_test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_all,
                     args.test_file_all,
                     args.train_feature_file,
                     args.test_feature_file,
                     args.magic_train_feature_file,
                     args.magic_test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

