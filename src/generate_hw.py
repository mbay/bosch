#!/usr/bin/env python
from __future__ import division
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
from scipy.sparse import csr_matrix

from kaggler.data_io import load_data, save_data


def _generate_feature(raw_file, feature_file, colnames, index_date_features):
    logging.info('loading raw data')
    X, y = load_data(raw_file)

    # skip ID
    X = X[:, 1:]
    
    # convert date variables into hours of week and weekdays
    logging.info('adding the hour and weekday features')
    X_d = (X[:, index_date_features].todense() * 10).astype(np.int64)        # in hour
    X_h = (X_d % 24)            # hours of the week
    X_w = ((X_d // 24) % 7)     # weekdays
 
    logging.debug('dim(X): {}'.format(X.shape))
    logging.debug('dim(X_h): {}'.format(X_h.shape))
    logging.debug('dim(X_w): {}'.format(X_w.shape))

    logging.info('saving features')
    X = csr_matrix(np.hstack((X_h, X_w)))
    logging.debug('dim(X): {}'.format(X.shape))

    save_data(X, y, feature_file)


def generate_feature(train_file_all, test_file_all, train_feature_file, test_feature_file):
    logging.info('loading column names')
    colnames = pd.read_csv('data/colnames.txt', header=None, squeeze=True)
    colnames = colnames[1:]
    colnames.index = range(0, len(colnames))

    index_date_features = colnames.index[colnames.apply(lambda x: True if 'D' in x else False)].values

    logging.info('generating features for training data')
    _generate_feature(train_file_all, train_feature_file, colnames, index_date_features)

    logging.info('generating features for test data')
    _generate_feature(test_file_all, test_feature_file, colnames, index_date_features)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-all', required=True, dest='train_file_all')
    parser.add_argument('--test-file-all', required=True, dest='test_file_all')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_all,
                     args.test_file_all,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

