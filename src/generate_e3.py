#!/usr/bin/env python
from __future__ import division
from scipy import sparse
from sklearn.datasets import dump_svmlight_file, load_svmlight_file
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from xgboost import XGBClassifier
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
from const import N_CLASS, SEED
from scipy.sparse import csr_matrix, hstack

from collections import defaultdict
from kaggler.data_io import load_data, save_data
from datetime import datetime


def generate_feature(ls_train_file,
                     ls_test_file,
                     train_feature_name,
                     test_feature_name):

    logging.info('loading train/test line-station data')
    # ls_trn, y_trn = load_data('./ls.trn.h5')
    # ls_tst, y_tst = load_data('./ls.tst.h5')
    ls_trn, y_trn = load_data(ls_train_file)
    ls_tst, y_tst = load_data(ls_test_file)

    # train product family dictionary (key, value) = (product_id, [row_id, rate])
    start = datetime.now()
    product_family_trn_dict = defaultdict(list)
    for row_id in range(ls_trn.shape[0]):
        product_code = np.zeros(ls_trn.shape[1])
        product_code[ls_trn[row_id, :].indices] = 1
        product_str = ''.join([str(int(x)) for x in product_code])
        product_family_trn_dict[product_str] += [[row_id, y_trn[row_id]]]
    logging.info('took {} seconds to form product_family_trn matrix'.format((datetime.now()-start).total_seconds()))    

    # test product family dictionary (key, value) = (product_id, [row_id])
    start = datetime.now()
    product_family_tst_dict = defaultdict(list)
    for row_id in range(ls_tst.shape[0]):
        product_code = np.zeros(ls_tst.shape[1])
        product_code[ls_tst[row_id, :].indices] = 1
        product_str = ''.join([str(int(x)) for x in product_code])
        product_family_tst_dict[product_str] += [row_id]
    logging.info('took {} seconds to form product_family_tst matrix'.format((datetime.now()-start).total_seconds()))

    # log in some info
    tst_products = set(product_family_tst_dict.keys())
    trn_products = set(product_family_trn_dict.keys())

    logging.info('number of products in train: {}'.format(len(trn_products)))
    logging.info('number of products in test: {}'.format(len(tst_products)))
    logging.info('tst-trn difference: {}'.format(len(tst_products)-len(trn_products)))

    missing_products = tst_products.difference(trn_products)
    logging.info('number of test products missing in train: {}'.format(len(missing_products)))

    missing_ids = []
    for product in missing_products:
        missing_ids += product_family_tst_dict[product]
    logging.info('number of test ids from missing products: {}'.format(len(missing_ids)))

    # for each product family in the train set calculate the error rate
    logging.info('calculating product stats for train data')
    start = datetime.now()
    product_stats_trn = pd.DataFrame(index=product_family_trn_dict.keys(), 
                                     columns=['row_ids','part_cnt', 'error_min', 'error_max','error_avg','error_med','error_std'])
    product_stats_trn.index.name = 'product'
    for product_id in product_family_trn_dict:

        product_vals = product_family_trn_dict[product_id]

        product_stats_trn.ix[product_id, 'row_ids'] = str([x[0] for x in product_vals])

        product_rates = [x[1] for x in product_vals]
        product_stats_trn.ix[product_id, 'part_cnt'] = len(product_rates)
        product_stats_trn.ix[product_id,'error_min'] = np.min(product_rates)
        product_stats_trn.ix[product_id,'error_max'] = np.max(product_rates)
        product_stats_trn.ix[product_id,'error_avg'] = np.mean(product_rates)
        product_stats_trn.ix[product_id,'error_med'] = np.median(product_rates)
        product_stats_trn.ix[product_id,'error_std'] = np.std(product_rates)
    print 'took {} seconds to product stats train.'.format( (datetime.now()-start).total_seconds())

    # product features for train data
    logging.info('building product features for train data')

    # re-build all train row_ids to be used in product features as index
    all_trn_ids = []
    for product in product_stats_trn.index:
        all_trn_ids += eval(product_stats_trn.ix[product, 'row_ids'])

    product_feature_names = ['part_cnt', 'error_min', 'error_max', 'error_avg', 'error_med', 'error_std']

    start = datetime.now()
    for feat_name in product_feature_names:
        y_trn_feature = pd.DataFrame(index=all_trn_ids, columns=[feat_name])
        for product in product_stats_trn.index:
            trn_ids = eval(product_stats_trn.ix[product, 'row_ids'])
            y_trn_feature.ix[trn_ids, feat_name] = product_stats_trn.ix[product, feat_name]
        y_trn_feature.sort_index(inplace=True)
        logging.info(len(y_trn_feature))
        y_trn_feature.to_csv(train_feature_name.replace('.trn.sps', '_'+feat_name+'.trn.sps'))
    logging.info('took {} seconds to create train features.'.format( (datetime.now()-start).total_seconds()))

    # product features for test data
    logging.info('building product features for test data')

    # re-build all test row_ids to be used in product features as index
    all_tst_ids = []
    for product in product_family_tst_dict.keys():
        all_tst_ids += product_family_tst_dict[product]
    logging.info(len(all_tst_ids))

    product_feature_names = ['part_cnt', 'error_min', 'error_max', 'error_avg', 'error_med', 'error_std']

    start = datetime.now()
    for feat_name in product_feature_names:
        y_tst_feature = pd.DataFrame(index=all_tst_ids, columns=[feat_name])
        # missed_products = []
        
        y_trn_feat_avg_all = np.median(product_stats_trn[feat_name])
        for product in product_family_tst_dict.keys():
            tst_ids = product_family_tst_dict[product]

            # if the product id exists in the trn set get the avg error rate, 
            # otherwise assign the median value
            if product in product_stats_trn.index:
                y_tst_feature.ix[tst_ids, feat_name] = product_stats_trn.ix[product, feat_name]
            else:
                # missed_products += [product]
                y_tst_feature.ix[tst_ids, feat_name] = y_trn_feat_avg_all
                
        y_tst_feature.sort_index(inplace=True)

        # pd.DataFrame(missed_products).to_csv('./missed_products_feat_{}.txt'.format(feat_name))    

        logging.info(len(y_tst_feature))

        y_tst_feature.to_csv(test_feature_name.replace('.tst.sps', '_'+feat_name+'.tst.sps'))
    logging.info('took {} seconds to create test features.'.format( (datetime.now()-start).total_seconds()) )


    logging.info('finished building product stats features')


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--ls-train-file', required=True, dest='ls_train_file')
    parser.add_argument('--ls-test-file', required=True, dest='ls_test_file')
    parser.add_argument('--train-feature-name', required=True, dest='train_feature_name')
    parser.add_argument('--test-feature-name', required=True, dest='test_feature_name')

    args = parser.parse_args()

    start_all = time.time()
    generate_feature(args.ls_train_file,
                     args.ls_test_file,
                     args.train_feature_name,
                     args.test_feature_name)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start_all))
