#!/usr/bin/env python
from __future__ import division
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time

from kaggler.data_io import load_data, save_data


def _generate_feature(raw_file, feature_file):
    logging.info('loading raw data')
    X, y = load_data(raw_file)

    # skip ID
    X = X[:, 1:]
    logging.debug('dim(X): {}'.format(X.shape))

    logging.info('saving features')
    save_data(X, y, feature_file)


def generate_feature(train_file_all, test_file_all, train_feature_file, test_feature_file):
    logging.info('generating features for training data')
    _generate_feature(train_file_all, train_feature_file)

    logging.info('generating features for test data')
    _generate_feature(test_file_all, test_feature_file)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-all', required=True, dest='train_file_all')
    parser.add_argument('--test-file-all', required=True, dest='test_file_all')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_all,
                     args.test_file_all,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

