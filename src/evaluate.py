#!/usr/bin/env python
from __future__ import division
from sklearn.metrics import matthews_corrcoef, roc_auc_score
import argparse
import json
import numpy as np
import os


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--target-file', '-t', required=True, dest='target_file')
    parser.add_argument('--predict-file', '-p', required=True, dest='predict_file')

    args = parser.parse_args()

    p = np.loadtxt(args.predict_file, delimiter=',')
    y = np.loadtxt(args.target_file, delimiter=',')

    model_name = os.path.basename(args.predict_file)[:-8]

    # pick the best threshold out-of-fold
    thresholds = np.linspace(0.01, 0.99, 100)
    mcc = np.array([matthews_corrcoef(y, p>thr) for thr in thresholds])
    best_threshold = thresholds[mcc.argmax()]

    print('{}\t{:.6f}\t{:6f}\t{:6f}'.format(model_name, roc_auc_score(y, p), mcc.max(), best_threshold))
