from external_call import run_command
from itertools import izip
import numpy as np
import os


class VwWrapper(object):

    def __init__(self, config):

        for key, value in config.iteritems():
            if (key != 'moniker') and (key != 'loglog'):
                config['moniker'] += '_{}={}'.format(key, value)
                config['moniker'] = config['moniker'][:min(len(config['moniker']), 150)]

        self.remove_temp_files = config['remove_temp_files']
        self.train_file = os.getcwd() + '/' + config['moniker'] + '.train.txt'
        self.model_file = os.getcwd() + '/' + config['moniker'] + '.model'
        self.cache_file = os.getcwd() + '/' + config['moniker'] + '.cache'
        self.input_prediction_file = os.getcwd() + '/' + config['moniker'] + '.input_predict.txt'
        self.output_prediction_file = os.getcwd() + '/' + config['moniker'] + '.output_predict.txt'

        self.train_command = 'vw -d ' + self.train_file + ' -f ' \
                                      + self.model_file + ' --cache_file ' \
                                      + self.cache_file + ' --holdout_off -k -b 24'

        non_vw_params = ['algo',
                         'moniker',
                         'loglog',
                         'remove_temp_files',
                         'interaction',
                         'hidden_units']

        if (config['algo'] == 'lr') or (config['algo'] == 'ftrl'):
            self.train_command += ' --{}'.format(config['algo'])
            for key, value in config.iteritems():
                if key not in non_vw_params:
                    self.train_command += ' --{} {}'.format(key, value)
        elif config['algo'] == 'nn':
            self.train_command += ' --nn {}'.format(config['hidden_units'])
        else:
            raise NotImplementedError('vw algorithm {} is not implemented'.format(config['algo']))

        if config['interaction'] == 'True':
            self.train_command += ' -q ::'

        self.predict_command = 'vw -i ' + self.model_file + ' -t ' + self.input_prediction_file \
                               + ' -p ' + self.output_prediction_file + ' --quiet'

    def __del__(self):
        if self.remove_temp_files:
            os.remove(self.train_file)
            os.remove(self.model_file)
            os.remove(self.cache_file)
            os.remove(self.input_prediction_file)
            os.remove(self.output_prediction_file)

    def fit(self, X, y):
        with open(self.train_file, 'w') as f:
            #for target, features in izip(y, X):
            for i in range(0, X.shape[0]):
                vw_row = []
                target = y[i]
                features = X[i,:].todense().tolist()[0]
                for j, value in enumerate(features):
                    if value != 0:
                        vw_row.append('{}:{}'.format(j, value))
                f.write('{} |'.format(target) + ' '.join(vw_row) + '\n')

        run_command(self.train_command)

    def predict(self, X):
        with open(self.input_prediction_file, 'w') as f:
            for i in range(0, X.shape[0]):
                vw_row = []
                features = X[i,:].todense().tolist()[0]
                for j, value in enumerate(features):
                    if value != 0:
                        vw_row.append('{}:{}'.format(j, value))
                f.write('|' + ' '.join(vw_row) + '\n')

        run_command(self.predict_command)

        return np.loadtxt(self.output_prediction_file)
