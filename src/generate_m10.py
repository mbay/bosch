#!/usr/bin/env python
from __future__ import division
from scipy import sparse
from scipy.sparse import csr_matrix, hstack

from sklearn.datasets import dump_svmlight_file, load_svmlight_file
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from xgboost import XGBClassifier
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
from const import N_CLASS, SEED


def generate_feature(train_file_all,
                     test_file_all,
                     train_feature_file,
                     test_feature_file):
    """Using m5 features + https://www.kaggle.com/alexxanderlarko/bosch-production-line-performance/road-2-0-4-featureset/code """
    logging.info('loading all train set')

    X, y_trn = load_svmlight_file('build/feature/m5.trn.sps')
    f_magic = pd.read_csv('data/Farons_train_features.csv.gz')
    X = hstack((X[:, 1:], f_magic.iloc[:, 1:].values), format='csr')

    clf = XGBClassifier(base_score=0.005, seed=SEED)
    clf.fit(X, y_trn)

    important_indices = np.where(clf.feature_importances_ > 0.001)[0]
    print(important_indices)
    X_trn = X[:, important_indices]

    # load test data
    X, y_tst = load_svmlight_file('build/feature/m5.tst.sps')
    f_magic = pd.read_csv('data/Farons_test_features.csv.gz')
    X = hstack((X[:, 1:], f_magic.iloc[:, 1:].values), format='csr')
    X_tst = X[:, important_indices]
    logging.debug('all features: train: {}, test: {}'.format(X_trn.shape, X_tst.shape))

    logging.info('saving as libSVM format')
    dump_svmlight_file(X_trn, y_trn, train_feature_file, zero_based=False)
    dump_svmlight_file(X_tst, y_tst, test_feature_file, zero_based=False)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-all', required=True, dest='train_file_all')
    parser.add_argument('--test-file-all', required=True, dest='test_file_all')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_all,
                     args.test_file_all,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

