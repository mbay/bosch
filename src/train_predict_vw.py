#!/usr/bin/env python

from __future__ import division
from sklearn.metrics import log_loss

import argparse
import logging
import numpy as np
import os
import time

from const import N_CLASS, SEED
from kaggler.data_io import load_data
#from vw import VwWrapper
from data import load_hdf5
from vw import VwWrapper

def train_predict(train_file, test_file, predict_valid_file, predict_test_file,
                  cv_id_file,
                  n_est, depth, n_fold=5):

    default_config = {'algo': 'ftrl',
                  'initial_t': '0',
                  'interaction': 'False',
                  'ftrl_alpha': '0.05',
                  'ftrl_beta': '0.8',
                  'learning_rate': '0.01',
                  'loglog': 'True',
                  'loss_function': 'logistic',
                  'moniker': 'vw_mon',
                  'passes': '2',
                  'power_t': '0.1',
                  'remove_temp_files': 'True'}

    feature_name = os.path.basename(train_file)[:-4]
    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG,
                        filename='vw_{}_{}_{}.log'.format(n_est,
                                                          depth,
                                                          feature_name))


    logging.info('Loading {} training and test data...'.format(train_file))
    X, y = load_data(train_file)
    y[y==0] = -1
    X_tst, _ = load_data(test_file)

    vw = VwWrapper(default_config)
    logging.info('Loading CV Ids. VW train command {}'.format(vw.train_command))
    cv_id = np.loadtxt(cv_id_file)

    logging.info('Cross validation...')
    #P_val = np.zeros((X.shape[0], N_CLASS))
    P_val = np.zeros(X.shape[0])
    P_tst = np.zeros(X_tst.shape[0])
    for i in range(1, n_fold + 1):
        logging.info("cv %d" % i)
        i_trn = np.where(cv_id != i)[0]
        i_val = np.where(cv_id == i)[0]
        logging.debug('valid: {}'.format(X[i_val].shape))
        logging.debug(len(set(y[i_trn])))

        vw.fit(X[i_trn], y[i_trn])
        pred = 1 / (1 + np.exp(-vw.predict(X[i_trn])))

        logging.info('val shape {}'.format(pred.shape))
        P_val[i_val] = pred

    logging.info('Retraining with 100% data...')
    vw.fit(X, y)
    pred = 1 / (1 + np.exp(-vw.predict(X_tst)))
    logging.info('test shape {}'.format(pred.shape))
    P_tst = pred

    logging.info('Saving predictions...')
    np.savetxt(predict_valid_file, P_val, fmt='%.6f', delimiter=',')
    np.savetxt(predict_test_file, P_tst, fmt='%.6f', delimiter=',')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file', required=True, dest='train_file')
    parser.add_argument('--test-file', required=True, dest='test_file')
    parser.add_argument('--predict-valid-file', required=True,
                        dest='predict_valid_file')
    parser.add_argument('--predict-test-file', required=True,
                        dest='predict_test_file')
    parser.add_argument('--cv-id', required=True, dest='cv_id_file')
    parser.add_argument('--n-est', default=100, type=int, dest='n_est')
    parser.add_argument('--depth', default=None, type=int, dest='depth')

    args = parser.parse_args()

    start = time.time()
    train_predict(train_file=args.train_file,
                  test_file=args.test_file,
                  predict_valid_file=args.predict_valid_file,
                  predict_test_file=args.predict_test_file,
                  cv_id_file=args.cv_id_file,
                  n_est=args.n_est,
                  depth=args.depth)
    logging.info('finished ({:.2f} min elasped)'.format((time.time() - start) /
                                                        60))
