#!/usr/bin/env python
from __future__ import division
from scipy import sparse
from sklearn.datasets import dump_svmlight_file, load_svmlight_file
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from xgboost import XGBClassifier
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
from const import N_CLASS, SEED
from scipy.sparse import csr_matrix, hstack, vstack
from kaggler.data_io import load_data, save_data


def generate_feature(train_file_all,
                     test_file_all,
                     train_feature_file,
                     test_feature_file):
    """Generate binary indicator features for product """
    logging.info('loading all train set')

    X_ls_trn, y_trn = load_data('build/feature/ls.trn.h5')
    X_ls_tst, y_tst = load_data('build/feature/ls.tst.h5')
    X_ls_trn_tst = vstack((X_ls_trn, X_ls_tst),  format='csr').todense()

    product_binary = pd.Series(index=range(0, X_ls_trn_tst.shape[0]), dtype=str)
    for idx in range(0, X_ls_trn_tst.shape[0]):
        product_binary[idx] = ''.join(str(int(elem)) for elem in X_ls_trn_tst[idx, :].tolist()[0])
    product_binary_enc = pd.get_dummies(product_binary)

    X_trn = csr_matrix(product_binary_enc.iloc[:X_ls_trn.shape[0], :])
    X_tst = csr_matrix(product_binary_enc.iloc[X_ls_trn.shape[0]:, :])

    logging.debug('all features: train: {}, test: {}'.format(X_trn.shape, X_tst.shape))

    logging.info('saving as libSVM format')
    save_data(X_trn, y_trn, train_feature_file)
    save_data(X_tst, y_tst, test_feature_file)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-all', required=True, dest='train_file_all')
    parser.add_argument('--test-file-all', required=True, dest='test_file_all')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_all,
                     args.test_file_all,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

