#!/usr/bin/env python
from __future__ import division
from scipy.sparse import csr_matrix, hstack
from sklearn.datasets import dump_svmlight_file, load_svmlight_file

import argparse
import logging
import time
from kaggler.data_io import load_data, save_data
import pandas as pd
from data import load_hdf5


def generate_feature(train_file_all,
                     test_file_all,
                     train_feature_file,
                     test_feature_file):
    """Adding features from  MrBoo https://www.kaggle.com/kagglemrboo/bosch-production-line-performance/newscr/run/418129/code """
    logging.info('loading all train set')
    print train_file_all
    X, y_trn = load_data(train_file_all)
    X = X[:, 1:]  # do not include Id

    X_xi_trn, y = load_data('build/feature/xi_keras.trn.h5')
    X_lt_trn, y = load_hdf5('build/feature/lt.trn.h5')
    X_magic_trn, y = load_hdf5('build/feature/magic2.trn.h5')
    X_trn = hstack((X_xi_trn, X_lt_trn, X_magic_trn), format='csr')

    # load test data
    X, y_tst = load_data(test_file_all)
    X = X[:, 1:]

    X_xi_tst, y = load_data('build/feature/xi_keras.tst.h5')
    X_lt_tst, y = load_hdf5('build/feature/lt.tst.h5')
    X_magic_tst, y = load_hdf5('build/feature/magic2.tst.h5')
    X_tst = hstack((X_xi_tst, X_lt_tst, X_magic_tst), format='csr')

    logging.debug('all features: train: {}, test: {}'.format(X_trn.shape, X_tst.shape))

    logging.info('saving as libSVM format')
    save_data(X_trn, y_trn, train_feature_file)
    save_data(X_tst, y_tst, test_feature_file)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-all', required=True, dest='train_file_all')
    parser.add_argument('--test-file-all', required=True, dest='test_file_all')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_all,
                     args.test_file_all,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

