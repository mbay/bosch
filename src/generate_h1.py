#!/usr/bin/env python
from __future__ import division
from scipy import sparse
from sklearn.datasets import dump_svmlight_file
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from xgboost import XGBClassifier
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
from const import N_CLASS, SEED


def generate_feature(train_file_num,
                     test_file_num,
                     train_file_date,
                     test_file_date,
                     train_feature_file,
                     test_feature_file):
    """Generate features from script https://www.kaggle.com/joconnor/bosch-production-line-performance/python-xgboost-starter-0-209-public-mcc """
    logging.info('load subset of data to get feature importance')

    date_chunks = pd.read_csv(train_file_date, index_col=0, chunksize=100000, dtype=np.float32)
    num_chunks = pd.read_csv(train_file_num, index_col=0,
                             usecols=list(range(969)), chunksize=100000, dtype=np.float32)
    X = pd.concat([pd.concat([dchunk, nchunk], axis=1).sample(frac=0.05, random_state=SEED)
                   for dchunk, nchunk in zip(date_chunks, num_chunks)])
    y = pd.read_csv(train_file_num, index_col=0, usecols=[0,969], dtype=np.float32).loc[X.index].values.ravel()
    X = X.values

    clf = XGBClassifier(base_score=0.005, seed=SEED)
    clf.fit(X, y)

    # threshold for a manageable number of features
    important_indices = np.where(clf.feature_importances_>0.005)[0]
    print(important_indices)

    logging.info('loading raw data files')
    n_date_features = 1156
    X_trn = np.concatenate([
            pd.read_csv(train_file_date, index_col=0, dtype=np.float32,
                        usecols=np.concatenate([[0], important_indices[important_indices < n_date_features] + 1])).values,
            pd.read_csv(train_file_num, index_col=0, dtype=np.float32,
                        usecols=np.concatenate([[0], important_indices[important_indices >= n_date_features] + 1 - n_date_features])).values], axis=1)
    y_trn = pd.read_csv(train_file_num, index_col=0, dtype=np.float32, usecols=[0,969]).values.ravel()

    # load test data
    X_tst = np.concatenate([
            pd.read_csv(test_file_date, index_col=0, dtype=np.float32,
                        usecols=np.concatenate([[0], important_indices[important_indices<n_date_features]+1])).values,
            pd.read_csv(test_file_num, index_col=0, dtype=np.float32,
                        usecols=np.concatenate([[0], important_indices[important_indices>=n_date_features] +1 - n_date_features])).values], axis=1)

    logging.debug('all features: train: {}, test: {}'.format(X_trn.shape, X_tst.shape))

    logging.info('saving as libSVM format')
    dump_svmlight_file(np.nan_to_num(X_trn), y_trn, train_feature_file, zero_based=False)
    dump_svmlight_file(np.nan_to_num(X_tst), [0,]*len(X_tst), test_feature_file, zero_based=False)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-num', required=True, dest='train_file_num')
    parser.add_argument('--test-file-num', required=True, dest='test_file_num')
    parser.add_argument('--train-file-date', required=True, dest='train_file_date')
    parser.add_argument('--test-file-date', required=True, dest='test_file_date')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_num,
                     args.test_file_num,
                     args.train_file_date,
                     args.test_file_date,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

