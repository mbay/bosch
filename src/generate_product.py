import pandas as pd
import numpy as np
from scipy import sparse
import logging
import argparse
import time

from collections import defaultdict
from kaggler.data_io import load_data, save_data
from datetime import datetime


def generate_feature(ls_train_file,
                     ls_test_file,
                     train_feature_file,
                     test_feature_file):

    ls_trn, y_trn = load_data(ls_train_file)
    ls_tst, y_tst = load_data(ls_test_file)

    # train product family dictionary (key, value) = (product_id, [row_id, rate])
    start = datetime.now()
    product_family_trn_dict = defaultdict(list)
    for row_id in range(ls_trn.shape[0]):
        product_code = np.zeros(ls_trn.shape[1])
        product_code[ls_trn[row_id, :].indices] = 1
        product_str = ''.join([str(int(x)) for x in product_code])
        product_family_trn_dict[product_str] += [[row_id, y_trn[row_id]]]
    logging.info('took {} seconds to form product_family_trn matrix'.format((datetime.now()-start).total_seconds()))

    # test product family dictionary (key, value) = (product_id, [row_id])
    start = datetime.now()
    product_family_tst_dict = defaultdict(list)
    for row_id in range(ls_tst.shape[0]):
        product_code = np.zeros(ls_tst.shape[1])
        product_code[ls_tst[row_id, :].indices] = 1
        product_str = ''.join([str(int(x)) for x in product_code])
        product_family_tst_dict[product_str] += [row_id]
    logging.info('took {} seconds to form product_family_tst matrix'.format((datetime.now()-start).total_seconds()))

    # log in some info
    tst_products = set(product_family_tst_dict.keys())
    trn_products = set(product_family_trn_dict.keys())

    logging.info('number of products in train: {}'.format(len(trn_products)))
    logging.info('number of products in test: {}'.format(len(tst_products)))
    logging.info('tst-trn difference: {}'.format(len(tst_products)-len(trn_products)))

    missing_products = tst_products.difference(trn_products)
    logging.info('number of test products missing in train: {}'.format(len(missing_products)))

    missing_ids = []
    for product in missing_products:
        missing_ids += product_family_tst_dict[product]
    logging.info('number of test ids from missing products: {}'.format(len(missing_ids)))

    product_feature_names = ['part_cnt', 'error_min', 'error_max', 'error_avg', 'error_med', 'error_std']

    # for each product family in the train set calculate the error rate
    logging.info('calculating product stats for train data')
    start = datetime.now()
    product_stats_trn = pd.DataFrame(index=product_family_trn_dict.keys(),
                                     columns=['row_ids'] + product_feature_names)
    product_stats_trn.index.name = 'product'
    for product_id in product_family_trn_dict:

        product_vals = product_family_trn_dict[product_id]

        product_stats_trn.ix[product_id, 'row_ids'] = [x[0] for x in product_vals]

        product_rates = [x[1] for x in product_vals]
        product_stats_trn.ix[product_id, 'part_cnt'] = len(product_rates)
        product_stats_trn.ix[product_id, 'error_min'] = np.min(product_rates)
        product_stats_trn.ix[product_id, 'error_max'] = np.max(product_rates)
        product_stats_trn.ix[product_id, 'error_avg'] = np.mean(product_rates)
        product_stats_trn.ix[product_id, 'error_med'] = np.median(product_rates)
        product_stats_trn.ix[product_id, 'error_std'] = np.std(product_rates)
    logging.info('took {} seconds to product stats train.'.format((datetime.now()-start).total_seconds()))

    # re-build all train row_ids to be used in product features as index
    all_trn_ids = []
    for product in product_family_trn_dict.keys():
        all_trn_ids += [x[0] for x in product_family_trn_dict[product]]
    logging.info(len(all_trn_ids))

    start = datetime.now()
    trn_data = []
    trn_cols = []
    trn_rows = []
    for feat_idx, feat_name in enumerate(product_feature_names):
        for product in product_family_trn_dict.keys():
            trn_ids = [x[0] for x in product_family_trn_dict[product]]

            trn_rows += trn_ids
            trn_cols += [feat_idx]*len(trn_ids)
            trn_data.append([product_stats_trn.ix[product, feat_name]]*len(trn_ids))
    product_trn_features = sparse.csc_matrix((np.concatenate(trn_data), (trn_rows, trn_cols)),
                                             shape=(ls_trn.shape[0], len(product_feature_names)))
    logging.info('took {} seconds to create train product features.'.format((datetime.now()-start).total_seconds()))

    # re-build all test row_ids to be used in product features as index
    all_tst_ids = []
    for product in product_family_tst_dict.keys():
        all_tst_ids += product_family_tst_dict[product]
    logging.info(len(all_tst_ids))

    start = datetime.now()
    tst_data = []
    tst_cols = []
    tst_rows = []
    for feat_idx, feat_name in enumerate(product_feature_names):
        y_trn_feat_med = np.median(product_stats_trn[feat_name])
        for product in product_family_tst_dict.keys():
            tst_ids = product_family_tst_dict[product]

            tst_rows += tst_ids
            tst_cols += [feat_idx]*len(tst_ids)
            if product in product_stats_trn.index:
                data = [product_stats_trn.ix[product, feat_name]]*len(tst_ids)
            else:
                data = [y_trn_feat_med]*len(tst_ids)
            tst_data.append(data)
    product_tst_features = sparse.csc_matrix((np.concatenate(tst_data), (tst_rows, tst_cols)),
                                             shape=(ls_tst.shape[0], 6))
    logging.info('took {} seconds to create test product features.'.format((datetime.now()-start).total_seconds()))

    logging.info('saving as in the HDF5 format')
    save_data(product_trn_features, y_trn, train_feature_file)
    save_data(product_tst_features, y_tst, test_feature_file)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--ls-train-file', required=True, dest='ls_train_file')
    parser.add_argument('--ls-test-file', required=True, dest='ls_test_file')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start_all = time.time()
    generate_feature(args.ls_train_file,
                     args.ls_test_file,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elapsed)'.format(time.time() - start_all))

