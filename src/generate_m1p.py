#!/usr/bin/env python
from __future__ import division
from scipy import sparse
from scipy.sparse import csr_matrix, hstack

from sklearn.datasets import dump_svmlight_file, load_svmlight_file
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from xgboost import XGBClassifier
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
from const import N_CLASS, SEED

from kaggler.data_io import load_data, save_data


def generate_feature(train_file_m1,
                     test_file_m1,
                     train_file_prod,
                     test_file_prod,
                     train_feature_file,
                     test_feature_file):

    logging.info('loading all train set')

    X_m1_trn, y_trn = load_data(train_file_m1)
    X_m1_tst, y_tst = load_data(test_file_m1)

    X_prod_trn, y_trn = load_data(train_file_prod)
    X_prod_tst, y_tst = load_data(test_file_prod)

    X_trn = hstack((X_m1_trn, X_prod_trn), format='csr')
    X_tst = hstack((X_m1_tst, X_prod_tst), format='csr')

    logging.debug('all features: train: {}, test: {}'.format(X_trn.shape, X_tst.shape))

    logging.info('saving as libSVM format')
    dump_svmlight_file(X_trn, y_trn, train_feature_file, zero_based=False)
    dump_svmlight_file(X_tst, y_tst, test_feature_file, zero_based=False)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-m1', required=True, dest='train_file_m1')
    parser.add_argument('--test-file-m1', required=True, dest='test_file_m1')
    parser.add_argument('--train-file-prod', required=True, dest='train_file_prod')
    parser.add_argument('--test-file-prod', required=True, dest='test_file_prod')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_m1,
                     args.test_file_m1,
                     args.train_file_prod,
                     args.test_file_prod,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

