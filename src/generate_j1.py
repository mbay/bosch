#!/usr/bin/env python
from __future__ import division
from sklearn.datasets import dump_svmlight_file, load_svmlight_file
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from xgboost import XGBClassifier
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
from const import N_CLASS, SEED
from scipy.sparse import hstack


get_station = lambda x: x.split('_')[1]


def _generate_feature(raw_file, feature_file, colnames, index_date_features, stations):
    logging.info('loading raw data')
    X, y = load_svmlight_file(raw_file)

    # skip ID
    X = X[:, 1:]
    
    logging.info('adding the station features')
    X_s = np.zeros((X.shape[0], len(stations)), dtype=bool)
    for idx, station in enumerate(stations):
        idx_station = colnames.index[colnames.apply(lambda x: True if station in x else False)].values
        X_s[:, idx] = np.array(X[:, idx_station].sum(axis=1)).flatten()

    # convert date variables into hours of week and weekdays
    logging.info('adding the hour and weekday features')
    X_d = (X[:, index_date_features].todense() * 10).astype(np.int64)        # in hour
    X_h = (X_d % 24)            # hours of the week
    X_w = ((X_d // 24) % 7)     # weekdays
 
    logging.debug('dim(X): {}'.format(X.shape))
    logging.debug('dim(X_h): {}'.format(X_h.shape))
    logging.debug('dim(X_w): {}'.format(X_w.shape))
    logging.debug('dim(X_s): {}'.format(X_s.shape))

    logging.info('saving features as libSVM format')
    X = hstack((X, X_h, X_w, X_s.astype(int)), format='csr')
    logging.debug('dim(X): {}'.format(X.shape))

    dump_svmlight_file(X, y, feature_file, zero_based=False)


def generate_feature(train_file_all, test_file_all, train_feature_file, test_feature_file):
    logging.info('loading column names')
    colnames = pd.read_csv('data/colnames.txt', header=None, squeeze=True)
    colnames = colnames[1:]
    colnames.index = range(0, len(colnames))

    index_date_features = colnames.index[colnames.apply(lambda x: True if 'D' in x else False)].values
    stations = sorted(colnames.apply(get_station).unique())

    logging.info('generating features for training data')
    _generate_feature(train_file_all, train_feature_file, colnames, index_date_features, stations)

    logging.info('generating features for test data')
    _generate_feature(test_file_all, test_feature_file, colnames, index_date_features, stations)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-all', required=True, dest='train_file_all')
    parser.add_argument('--test-file-all', required=True, dest='test_file_all')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_all,
                     args.test_file_all,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

