import numpy as np
import pandas as pd
from xgboost import XGBClassifier
from sklearn.metrics import matthews_corrcoef, roc_auc_score
from sklearn.cross_validation import cross_val_score, StratifiedKFold
from sklearn.datasets import load_svmlight_file

# Baseline submission using the code at  https://www.kaggle.com/joconnor/bosch-production-line-performance/python-xgboost-starter-0-209-public-mcc/comments

trn_file = '../data/train_raw.sps'
X, y = load_svmlight_file(trn_file)
X = X[:,1:] # do not include Id


#clf = XGBClassifier(max_depth=5, base_score=0.005)
clf = XGBClassifier(colsample_bytree=0.5, gamma=0,
       learning_rate=0.1, max_delta_step=0, max_depth=5,
       min_child_weight=1, n_estimators=100, nthread=-1,
       objective='binary:logistic', seed=0, silent=True, subsample=0.5)

cv = StratifiedKFold(y, n_folds=3)
preds = np.ones(y.shape[0])
for i, (train, test) in enumerate(cv):
    preds[test] = clf.fit(X[train], y[train]).predict_proba(X[test])[:,1]
    print("fold {}, ROC AUC: {:.3f}".format(i, roc_auc_score(y[test], preds[test])))
print(roc_auc_score(y, preds))

# pick the best threshold out-of-fold
thresholds = np.linspace(0.01, 0.99, 50)
mcc = np.array([matthews_corrcoef(y, preds>thr) for thr in thresholds])
best_threshold = thresholds[mcc.argmax()]
print(mcc.max())


tst_file = '../data/test_raw.sps'
X, y = load_svmlight_file(tst_file)
X = X[:,1:]

preds = (clf.predict_proba(X)[:,1] > best_threshold).astype(np.int8)
# and submit
sub = pd.read_csv("../data/sample_submission.csv", index_col=0)
sub["Response"] = preds
sub.to_csv("submission_v2.csv.gz", compression="gzip")


