import h5py
from scipy import sparse

def load_hdf5(path):
    with h5py.File(path, 'r') as f:
        shape = tuple(f['shape'][...])
        data = f['data'][...]
        indices = f['indices'][...]
        indptr = f['indptr'][...]
        y = f['target'][...]

    X = sparse.csr_matrix((data, indices, indptr), shape=shape)

    return X, y
