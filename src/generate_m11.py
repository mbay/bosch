#!/usr/bin/env python
from __future__ import division
from scipy.sparse import csr_matrix, hstack
from sklearn.datasets import dump_svmlight_file, load_svmlight_file

import argparse
import logging
import time
from kaggler.data_io import load_data, save_data


def generate_feature(train_file_all, test_file_all,
                     train_ls_feature_file, test_ls_feature_file,
                     train_magic_feature_file, test_magic_feature_file,
                     train_feature_file, test_feature_file):
    """Generate features from script https://www.kaggle.com/joconnor/bosch-production-line-performance/python-xgboost-starter-0-209-public-mcc """
    logging.info('loading all train set')
    print train_file_all
    X_trn, y_trn = load_data(train_file_all)
    X_trn = X_trn[:, 1:]  # do not include Id

    X_ls_trn, y = load_data(train_ls_feature_file)
    X_magic_trn, y = load_data(train_magic_feature_file)
    X_trn = hstack((X_trn, X_ls_trn, X_magic_trn), format='csr')

    # load test data
    X_tst, y_tst = load_data(test_file_all)
    X_tst = X_tst[:, 1:]

    X_ls_tst, y = load_data(test_ls_feature_file)
    X_magic_tst, y = load_data(test_magic_feature_file)
    X_tst = hstack((X_tst, X_ls_tst, X_magic_tst), format='csr')

    logging.debug('all features: train: {}, test: {}'.format(X_trn.shape, X_tst.shape))

    logging.info('saving as libSVM format')
    save_data(X_trn, y_trn, train_feature_file)
    save_data(X_tst, y_tst, test_feature_file)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-all', required=True, dest='train_file_all')
    parser.add_argument('--test-file-all', required=True, dest='test_file_all')
    parser.add_argument('--train-ls-feature-file', required=True, dest='train_ls_feature_file')
    parser.add_argument('--test-ls-feature-file', required=True, dest='test_ls_feature_file')
    parser.add_argument('--train-magic-feature-file', required=True, dest='train_magic_feature_file')
    parser.add_argument('--test-magic-feature-file', required=True, dest='test_magic_feature_file')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_all,
                     args.test_file_all,
                     args.train_ls_feature_file,
                     args.test_ls_feature_file,
                     args.train_magic_feature_file,
                     args.test_magic_feature_file,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

