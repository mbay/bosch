#!/usr/bin/env python
from __future__ import division
from scipy import sparse
from sklearn.datasets import dump_svmlight_file, load_svmlight_file
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from xgboost import XGBClassifier
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
from const import N_CLASS, SEED
from scipy.sparse import csr_matrix, hstack


def f(x):
    t = x.split('_')
    return '{}_{}'.format(t[0], t[1])


def g(x):
    t = x.split('_')
    return '{}'.format(t[1])


def generate_feature(train_file_all,
                     test_file_all,
                     train_feature_file,
                     test_feature_file):
    """Generate features from script https://www.kaggle.com/joconnor/bosch-production-line-performance/python-xgboost-starter-0-209-public-mcc """
    logging.info('loading all train set')

    X, y_trn = load_svmlight_file(train_file_all)
    logging.info('train set loaded\nprocessing train set...')

    X = X[:, 1:]  # do not include Id



    colnames = pd.read_csv('data/colnames.txt', header=None, squeeze=True)
    colnames = colnames[1:]
    colnames.index = range(0, len(colnames))

    row_l = pd.Series(index=np.unique(colnames.apply(f)))
    row_s = pd.Series(index=np.unique(colnames.apply(g)))
    all_l = np.empty(shape=(X.shape[0], len(row_l)))
    all_s = np.empty(shape=(X.shape[0], len(row_s)))

    dt_inds = range(926, 1088)
    dt_diff = np.empty(shape=(X.shape[0], 4))
    lines_dt = [[0, 37], [37, 123], [123, 127], [127, 162]]

    for idx in range(0, X.shape[0]):
        row = X[idx, :]
        nz = list(row.nonzero()[1])

        row_l = pd.Series(index=np.unique(colnames.apply(f)))
        nz_l = colnames[nz].apply(f).unique()
        row_l[nz_l] = 1
        all_l[idx, :] = row_l.values

        row_s = pd.Series(index=np.unique(colnames.apply(g)))
        nz_s = colnames[nz].apply(g).unique()
        row_s[nz_s] = 1
        all_s[idx, :] = row_s.values

        row_dt = np.squeeze(np.asarray(row[0,dt_inds].todense()))
        for i in range(0, 4):
            ldt = row_dt[lines_dt[i][0]:lines_dt[i][1]]
            if len(ldt[ldt > 0]) > 1:
                dt_diff[idx, i] = np.max(np.diff(np.sort(ldt[ldt > 0])))
            else:
                dt_diff[idx, i] = 0

    X = hstack((X, np.nan_to_num(all_l), np.nan_to_num(all_s), np.nan_to_num(dt_diff)), format='csr')

    clf = XGBClassifier(base_score=0.005, seed=SEED)
    clf.fit(X, y_trn)

    important_indices = np.where(clf.feature_importances_ > 0.001)[0]
    print(important_indices)
    X_trn = X[:, important_indices]

    logging.info('train set processed')

    # load test data
    logging.info('loading all test set')
    X, y_tst = load_svmlight_file(test_file_all)
    logging.info('test set loaded\nprocessing test set...')

    X = X[:, 1:]
    dt_diff = np.empty(shape=(X.shape[0], 4))
    all_l = np.empty(shape=(X.shape[0], len(row_l)))
    all_s = np.empty(shape=(X.shape[0], len(row_s)))
    for idx in range(0, X.shape[0]):
        row = X[idx, :]
        nz = list(row.nonzero()[1])

        row_l = pd.Series(index=np.unique(colnames.apply(f)))
        nz_l = colnames[nz].apply(f).unique()
        row_l[nz_l] = 1
        all_l[idx, :] = row_l.values

        row_s = pd.Series(index=np.unique(colnames.apply(g)))
        nz_s = colnames[nz].apply(g).unique()
        row_s[nz_s] = 1
        all_s[idx, :] = row_s.values

        row_dt = np.squeeze(np.asarray(row[0,dt_inds].todense()))
        for i in range(0, 4):
            ldt = row_dt[lines_dt[i][0]:lines_dt[i][1]]
            if len(ldt[ldt > 0]) > 1:
                dt_diff[idx, i] = np.max(np.diff(np.sort(ldt[ldt > 0])))
            else:
                dt_diff[idx, i] = 0

    X = hstack((X, np.nan_to_num(all_l), np.nan_to_num(all_s), np.nan_to_num(dt_diff)), format='csr')
    X_tst = X[:, important_indices]

    logging.info('test set processed')

    logging.debug('all features: train: {}, test: {}'.format(X_trn.shape, X_tst.shape))

    logging.info('saving as libSVM format')
    dump_svmlight_file(X_trn, y_trn, train_feature_file, zero_based=False)
    dump_svmlight_file(X_tst, y_tst, test_feature_file, zero_based=False)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-all', required=True, dest='train_file_all')
    parser.add_argument('--test-file-all', required=True, dest='test_file_all')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_all,
                     args.test_file_all,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

