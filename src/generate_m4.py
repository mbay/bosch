#!/usr/bin/env python
from __future__ import division
from scipy import sparse
from sklearn.datasets import dump_svmlight_file, load_svmlight_file
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from xgboost import XGBClassifier
import argparse
import logging
import numpy as np
import os
import pandas as pd
import time
from const import N_CLASS, SEED
from scipy.sparse import csr_matrix, hstack



def f(x):
    t = x.split('_')
    return '{}_{}'.format(t[0], t[1])


def generate_feature(train_file_all,
                     test_file_all,
                     train_feature_file,
                     test_feature_file):
    """Generate features from script https://www.kaggle.com/joconnor/bosch-production-line-performance/python-xgboost-starter-0-209-public-mcc """
    logging.info('loading all train set')

    X, y_trn = load_svmlight_file(train_file_all)
    X = X[:, 1:]  # do not include Id

    clf = XGBClassifier(base_score=0.005, seed=SEED)
    clf.fit(X, y_trn)

    important_indices = np.where(clf.feature_importances_ > 0.001)[0]
    print(important_indices)
    X = X[:, important_indices]

    colnames = pd.read_csv('data/colnames.txt', header=None, squeeze=True)
    colnames = colnames[1:]
    colnames.index = range(0, len(colnames))

    row_ls = pd.Series(index=np.unique(colnames.apply(f)))
    all_ls = np.empty(shape=(X.shape[0], len(row_ls)))
    for idx in range(0,X.shape[0]):
        row = X[idx, :]
        nz = list(row.nonzero()[1])
        row_ls = pd.Series(index=np.unique(colnames.apply(f)))
        nz_ls = colnames[nz].apply(f).unique()
        row_ls[nz_ls] = 1
        all_ls[idx, :] = row_ls.values

    X_trn = hstack((X, np.nan_to_num(all_ls)), format='csr')

    # load test data
    X, y_tst = load_svmlight_file(test_file_all)
    X = X[:, 1:]
    X = X[:, important_indices]

    row_ls = pd.Series(index=np.unique(colnames.apply(f)))
    all_ls = np.empty(shape=(X.shape[0], len(row_ls)))
    for idx in range(0,X.shape[0]):
        row = X[idx, :]
        nz = list(row.nonzero()[1])
        row_ls = pd.Series(index=np.unique(colnames.apply(f)))
        nz_ls = colnames[nz].apply(f).unique()
        row_ls[nz_ls] = 1
        all_ls[idx, :] = row_ls.values

    X_tst = hstack((X, np.nan_to_num(all_ls)), format='csr')

    logging.debug('all features: train: {}, test: {}'.format(X_trn.shape, X_tst.shape))

    logging.info('saving as libSVM format')
    dump_svmlight_file(X_trn, y_trn, train_feature_file, zero_based=False)
    dump_svmlight_file(X_tst, y_tst, test_feature_file, zero_based=False)


if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s   %(levelname)s   %(message)s',
                        level=logging.DEBUG)

    parser = argparse.ArgumentParser()
    parser.add_argument('--train-file-all', required=True, dest='train_file_all')
    parser.add_argument('--test-file-all', required=True, dest='test_file_all')
    parser.add_argument('--train-feature-file', required=True, dest='train_feature_file')
    parser.add_argument('--test-feature-file', required=True, dest='test_feature_file')

    args = parser.parse_args()

    start = time.time()
    generate_feature(args.train_file_all,
                     args.test_file_all,
                     args.train_feature_file,
                     args.test_feature_file)
    logging.info('finished ({:.2f} sec elasped)'.format(time.time() - start))

