#!/usr/bin/env python
from __future__ import division
from sklearn.metrics import matthews_corrcoef, roc_auc_score
import argparse
import json
import numpy as np
import pandas as pd
import os


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--target-file', '-t', required=True, dest='target_file')
    parser.add_argument('--predict-file', '-p', required=True, dest='predict_file')
    parser.add_argument('--tst-predict-file', '-tp', required=True, dest='test_predict_file')
    parser.add_argument('--submission-file', '-s', required=True, dest='submission_file')
    parser.add_argument('--sample-submission', '-ss', required=True, dest='sample_submission')

    args = parser.parse_args()

    p = np.loadtxt(args.predict_file, delimiter=',')
    y = np.loadtxt(args.target_file, delimiter=',')

    model_name = os.path.basename(args.predict_file)[:-8]

    # pick the best threshold out-of-fold
    thresholds = np.linspace(0.01, 0.99, 100)
    mcc = np.array([matthews_corrcoef(y, p>thr) for thr in thresholds])
    best_threshold = thresholds[mcc.argmax()]
    print(mcc.max())
    print best_threshold

    tst_p = np.loadtxt(args.test_predict_file, delimiter=',')
    preds = ( tst_p > best_threshold).astype(np.int8)
    # and submit
    sub = pd.read_csv(args.sample_submission, index_col=0)
    sub["Response"] = preds
    sub.to_csv(args.submission_file)
