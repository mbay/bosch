from subprocess import Popen, PIPE
import logging

def run_command(command):
    """ Run an external executable

    Args:
        command: the string to run in the commandline.
    """
    logging.info('\n running commmand:\n{}'.format(command))
    proc = Popen(command, stdout=PIPE, stderr=PIPE, shell=True)
    stdout, stderr = proc.communicate()
    if not proc.returncode:
        logging.debug('\n\nExternal run successful. stdout:\n{}\nstderr:{}\n'.format(stdout, stderr))
    else:
        logging.info('\n\nExternal run failed. Command ran: {} stdout: {}\nstderr: {}'.format(command, stdout, stderr))
        raise Exception("External run failed.")
    return stdout
