# packages
APT_PKGS := python-pip python-dev
BREW_PKGS := --python
PIP_PKGS := numpy scipy pandas scikit-learn

SED := sed

# directories
DIR_DATA := data
DIR_BUILD := build
DIR_BIN := $(DIR_BUILD)/bin
DIR_BLEND := $(DIR_BUILD)/blend
DIR_FEATURE := $(DIR_BUILD)/feature
DIR_METRIC := $(DIR_BUILD)/metric
DIR_MODEL := $(DIR_BUILD)/model

# directories for the cross validation and ensembling
DIR_VAL := $(DIR_BUILD)/val
DIR_TST := $(DIR_BUILD)/tst

DIRS := $(DIR_DATA) $(DIR_BUILD) $(DIR_FEATURE) $(DIR_METRIC) $(DIR_MODEL) \
        $(DIR_VAL) $(DIR_TST) $(DIR_BIN) $(DIR_BLEND)

# data files for training and predict
DATA_TRN_NUM_RAW := $(DIR_DATA)/train_numeric.csv
DATA_TST_NUM_RAW := $(DIR_DATA)/test_numeric.csv
DATA_TRN_DATE_RAW := $(DIR_DATA)/train_date.csv
DATA_TST_DATE_RAW := $(DIR_DATA)/test_date.csv
DATA_TRN_CAT_RAW := $(DIR_DATA)/train_categorical.csv
DATA_TST_CAT_RAW := $(DIR_DATA)/test_categorical.csv
DATA_TRN_NUM_PIK := $(DIR_DATA)/train_numeric.pik
DATA_TST_NUM_PIK := $(DIR_DATA)/test_numeric.pik
DATA_TRN_DATE_PIK := $(DIR_DATA)/train_date.pik
DATA_TST_DATE_PIK := $(DIR_DATA)/test_date.pik
DATA_TRN_CAT_PIK := $(DIR_DATA)/train_categorical.pik
DATA_TST_CAT_PIK := $(DIR_DATA)/test_categorical.pik
DATA_TRN_ALL := $(DIR_DATA)/train_raw.h5
DATA_TST_ALL := $(DIR_DATA)/test_raw.h5


SAMPLE_SUBMISSION := $(DIR_DATA)/sample_submission.csv

ID_TST := $(DIR_DATA)/id.tst.csv
HEADER := $(DIR_DATA)/header.csv
CV_ID := $(DIR_DATA)/cv_id.txt

Y_TRN:= $(DIR_FEATURE)/y.trn.txt
Y_TST:= $(DIR_FEATURE)/y.tst.txt

$(DIRS):
	mkdir -p $@

$(HEADER): $(SAMPLE_SUBMISSION)
	head -1 $< > $@

$(ID_TST): $(SAMPLE_SUBMISSION)
	cut -d, -f1 $< | tail -n +2 > $@

$(Y_TST): $(SAMPLE_SUBMISSION) | $(DIR_FEATURE)
	cut -d, -f2 $< | tail -n +2 > $@

$(Y_TRN) $(CV_ID): $(DATA_TRN_NUM_RAW) | $(DIR_FEATURE)
	python src/extract_target_cvid.py --train-file $< \
                                      --target-file $(Y_TRN) \
                                      --cvid-file $(CV_ID)

$(DATA_TRN_CAT_PIK): $(DATA_TRN_CAT_RAW)
	python src/convert_csv_to_pickle.py --input $< \
                                        --output $@

$(DATA_TST_CAT_PIK): $(DATA_TST_CAT_RAW)
	python src/convert_csv_to_pickle.py --input $< \
                                        --output $@

$(DATA_TRN_NUM_PIK): $(DATA_TRN_NUM_RAW)
	python src/convert_csv_to_pickle.py --input $< \
                                        --output $@

$(DATA_TST_NUM_PIK): $(DATA_TST_NUM_RAW)
	python src/convert_csv_to_pickle.py --input $< \
                                        --output $@

$(DATA_TRN_DATE_PIK): $(DATA_TRN_DATE_RAW)
	python src/convert_csv_to_pickle.py --input $< \
                                        --output $@
$(DATA_TST_DATE_PIK): $(DATA_TST_DATE_RAW)
	python src/convert_csv_to_pickle.py --input $< \
                                        --output $@

# cleanup
clean::
	find . -name '*.pyc' -delete

clobber: clean
	-rm -rf $(DIR_DATA) $(DIR_BUILD)

.PHONY: clean clobber mac.setup ubuntu.setup apt.setup pip.setup
