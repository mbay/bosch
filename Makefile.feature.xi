#--------------------------------------------------------------------------
# feature1: benchmark feature
#--------------------------------------------------------------------------
include Makefile

FEATURE_NAME := xi

FEATURE_TRN := $(DIR_FEATURE)/$(FEATURE_NAME).trn.h5
FEATURE_TST := $(DIR_FEATURE)/$(FEATURE_NAME).tst.h5
FEATURE1_TRN := $(DIR_FEATURE)/xi_dup.trn.h5
FEATURE1_TST := $(DIR_FEATURE)/xi_dup.tst.h5
FEATURE2_TRN := $(DIR_FEATURE)/xi_magic.trn.h5
FEATURE2_TST := $(DIR_FEATURE)/xi_magic.tst.h5

$(FEATURE_TRN): $(FEATURE1_TRN) $(FEATURE2_TRN) | $(DIR_FEATURE)
	python ./src/combine_features.py --feature-file1 $< \
                                     	 --feature-file2 $(lastword $^) \
                                     	 --feature-file $@

$(FEATURE_TST): $(FEATURE1_TST) $(FEATURE2_TST) | $(DIR_FEATURE)
	python ./src/combine_features.py --feature-file1 $< \
                                     	 --feature-file2 $(lastword $^) \
                                     	 --feature-file $@
