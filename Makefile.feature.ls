#--------------------------------------------------------------------------
# feature1: benchmark feature
#--------------------------------------------------------------------------
include Makefile

FEATURE_NAME := ls

FEATURE_TRN := $(DIR_FEATURE)/$(FEATURE_NAME).trn.h5
FEATURE_TST := $(DIR_FEATURE)/$(FEATURE_NAME).tst.h5

$(FEATURE_TRN) $(FEATURE_TST): $(DATA_TRN_ALL) $(DATA_TST_ALL) | $(DIR_FEATURE)
	python ./src/generate_$(FEATURE_NAME).py --train-file-all $< \
                                             --test-file-all $(lastword $^) \
                                             --train-feature-file $(FEATURE_TRN) \
                                             --test-feature-file $(FEATURE_TST)

all_feature: $(FEATURE_TRN)
