include Makefile.feature.m1p

N = 10000
DEPTH = 6
LRATE = 0.05
SUBCOL = 1
SUBROW = 0.7
SUBLEV = 0.5
WEIGHT = 1
N_STOP = 100
PRIOR = 0.5
ALGO_NAME := xg_$(N)_$(DEPTH)_$(LRATE)_$(SUBCOL)_$(SUBROW)_$(SUBLEV)_$(WEIGHT)_$(N_STOP)_$(PRIOR)
MODEL_NAME := $(ALGO_NAME)_$(FEATURE_NAME)

METRIC_VAL := $(DIR_METRIC)/$(MODEL_NAME).val.txt

PREDICT_VAL := $(DIR_VAL)/$(MODEL_NAME).val.yht
PREDICT_TST := $(DIR_TST)/$(MODEL_NAME).tst.yht

SUBMISSION_TST := $(DIR_TST)/$(MODEL_NAME).sub.csv
SUBMISSION_TST_GZ := $(DIR_TST)/$(MODEL_NAME).sub.csv.gz

all: validation submission
validation: $(METRIC_VAL)
submission: $(SUBMISSION_TST)
retrain: clean_$(ALGO_NAME) submission

$(PREDICT_TST) $(PREDICT_VAL): $(FEATURE_TRN) $(FEATURE_TST) $(CV_ID) \
                                   | $(DIR_VAL) $(DIR_TST)
	./src/train_predict_xg.py --train-file $< \
                              --test-file $(word 2, $^) \
                              --predict-valid-file $(PREDICT_VAL) \
                              --predict-test-file $(PREDICT_TST) \
                              --depth $(DEPTH) \
                              --lrate $(LRATE) \
                              --n-est $(N) \
                              --subcol $(SUBCOL) \
                              --subrow $(SUBROW) \
                              --sublev $(SUBLEV) \
                              --weight $(WEIGHT) \
                              --early-stop $(N_STOP) \
                              --prior $(PRIOR) \
                              --cv-id $(lastword $^)

$(SUBMISSION_TST_GZ): $(SUBMISSION_TST)
	gzip $<

$(SUBMISSION_TST): $(PREDICT_TST) $(PREDICT_VAL) $(Y_TRN) | $(DIR_TST)
	python ./src/submission.py --predict-file $(word 2, $^) \
                                --target-file $(word 3, $^) \
                                --tst-predict-file $< \
                                --submission-file $@ \
                                --sample-submission $(SAMPLE_SUBMISSION)

$(METRIC_VAL): $(PREDICT_VAL) $(Y_TRN) | $(DIR_METRIC)
	python ./src/evaluate.py --predict-file $< \
                             --target-file $(word 2, $^) > $@
	cat $@


clean:: clean_$(ALGO_NAME)

clean_$(ALGO_NAME):
	-rm $(METRIC_VAL) $(PREDICT_VAL) $(PREDICT_TST) $(SUBMISSION_TST)
	find . -name '*.pyc' -delete

.DEFAULT_GOAL := all
