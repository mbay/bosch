include Makefile.feature.xi_keras

N = 450
DEPTH = 40
ALGO_NAME := vw_ftrli_xi_keras_$(N)_$(DEPTH)
MODEL_NAME := $(ALGO_NAME)_$(FEATURE_NAME)

METRIC_VAL := $(DIR_METRIC)/$(MODEL_NAME).val.txt

PREDICT_VAL := $(DIR_VAL)/$(MODEL_NAME).val.yht
PREDICT_TST := $(DIR_TST)/$(MODEL_NAME).tst.yht

SUBMISSION_TST := $(DIR_TST)/$(MODEL_NAME).sub.csv
SUBMISSION_TST_GZ := $(DIR_TST)/$(MODEL_NAME).sub.csv.gz

all: validation submission
validation: $(METRIC_VAL)
submission: $(SUBMISSION_TST)
retrain: clean_$(ALGO_NAME) submission

$(PREDICT_TST) $(PREDICT_VAL): $(FEATURE_TRN) $(FEATURE_TST) $(CV_ID) \
                                   | $(DIR_VAL) $(DIR_TST)
	./src/train_predict_vw.py --train-file $< \
                                  --test-file $(word 2, $^) \
                                  --predict-valid-file $(PREDICT_VAL) \
                                  --predict-test-file $(PREDICT_TST) \
                                  --cv-id $(lastword $^) \
                                  --depth $(DEPTH) \
                                  --n-est $(N)

$(SUBMISSION_TST_GZ): $(SUBMISSION_TST)
	gzip $<

$(SUBMISSION_TST): $(PREDICT_TST) $(PREDICT_VAL) $(Y_TRN) | $(DIR_TST)
	python ./src/submission.py --predict-file $(word 2, $^) \
                                --target-file $(word 3, $^) \
                                --tst-predict-file $< \
                                --submission-file $@ \
                                --sample-submission $(SAMPLE_SUBMISSION)

$(METRIC_VAL): $(PREDICT_VAL) $(Y_TRN) | $(DIR_METRIC)
	python ./src/evaluate.py --predict-file $< \
                             --target-file $(word 2, $^) > $@
	cat $@


clean:: clean_$(ALGO_NAME)

clean_$(ALGO_NAME):
	-rm $(METRIC_VAL) $(PREDICT_VAL) $(PREDICT_TST) $(SUBMISSION_TST)
	find . -name '*.pyc' -delete

.DEFAULT_GOAL := all
